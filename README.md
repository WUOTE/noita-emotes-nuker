# noita-emotes-nuker
Add/remove Noita spells as 7tv emotes to/from your account FAST!
You stream Noita and you want your viewers suggest wand builds easily?
Now youd can do this:  

![image](./img/chat_example.png)

## Note
For some users their user id and emotes set id differ, at the moment addign emotes for such users will not work, we're working on a fix.

# Usage
If you have node.js installed, download [`index.js`](https://gitlab.com/WUOTE/noita-emotes-nuker/-/raw/main/index.js?inline=false) and run `npm install`, then `node .\index.js`
If you don't have node.js installed on your machine, download a precompiled binary for your OS (binaries were made using [pkg](https://github.com/vercel/pkg) and are available for [Windows](https://gitlab.com/WUOTE/noita-emotes-nuker/-/raw/main/bin/noita-emote-nuker.exe?inline=false), [Linux](https://gitlab.com/WUOTE/noita-emotes-nuker/-/raw/main/bin/noita-emote-nuker-linux?inline=false), [MacOS](https://gitlab.com/WUOTE/noita-emotes-nuker/-/raw/main/bin/noita-emote-nuker-macos?inline=false)) and run it.

# TODO
- [ ] Divide spells into two groups: basic spells / unlockable spells.
- [x] Break an acid flask.
- [ ] Utilize twitch integration to dynamically add unlocked spells' emotes.

# Credits
Thanks to myndzi, bolt_ for the help with the code. Thanks to rynthera, tanksyplays for the help with testing. Thanks to pood707, PlaceboEU, Flupperich, Nauk, ZaximusRex, bronwen_n, teh60 for helping me uploading the emotes.